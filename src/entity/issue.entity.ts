import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm'
import { Assignee } from './assignee.entity'
import { Comment } from './comment.entity'
import { IssueMessage } from './issue-message.entity'

@Entity()
export class Issue {
  @PrimaryColumn()
  id: number

  @Column()
  title: string

  @Column('text')
  description: string

  @Column()
  user: string

  @Column()
  state: string

  @Column()
  action: string

  @Column()
  url: string

  @ManyToMany(() => Assignee, {
    cascade: true,
  })
  @JoinTable()
  assignees: Assignee[]

  @OneToMany(() => Comment, (comment) => comment.issue)
  comments: Comment[]

  @OneToMany(() => IssueMessage, (issueMessage) => issueMessage.issue)
  messages: IssueMessage[]

  @CreateDateColumn()
  created_at: Date

  @UpdateDateColumn()
  updated_at: Date
}
