import { Module } from '@nestjs/common'

import { GitlabWebhookModule } from './gitlab-webhook/module'
import { CronModule } from './middleware/cron/module'
import { DotenvModule } from './middleware/dotenv/module'
import { TypeOrmConfigModule } from './typeorm-config/module'

@Module({
  imports: [TypeOrmConfigModule, GitlabWebhookModule, CronModule, DotenvModule],
})
export class AppModule {}
