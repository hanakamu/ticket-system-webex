import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { GitlabWebhookController } from './controller'
import { GitlabWebhookService } from './service'

import { Issue } from '../entity/issue.entity'
import { Comment } from '../entity/comment.entity'
import { WebexBotModule } from 'src/middleware/webex/bot/module'
import { IssueMessage } from 'src/entity/issue-message.entity'
import { CommentMessage } from 'src/entity/comment-message.entity'
import { Room } from 'src/entity/room.entity'

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Issue,
      Comment,
      IssueMessage,
      CommentMessage,
      Room,
    ]),
    WebexBotModule,
  ],
  controllers: [GitlabWebhookController],
  providers: [GitlabWebhookService],
})
export class GitlabWebhookModule {}
