import { Body, Controller, Get, Post } from '@nestjs/common'
import { GitlabWebhookService } from './service'

@Controller('gitlab-webhook')
export class GitlabWebhookController {
  constructor(private readonly service: GitlabWebhookService) {}

  @Get()
  rcvGet(): string {
    this.service.testWebexIntegration()
    return this.service.defaulMessage()
  }

  @Post()
  rcvWebhook(@Body() body) {
    this.service.saveData(body)
    this.service.sendWebexMessage(body)
  }
}
