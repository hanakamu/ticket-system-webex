import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  PrimaryColumn,
} from 'typeorm'
import { Comment } from './comment.entity'
import { Room } from './room.entity'

@Entity()
export class CommentMessage {
  @PrimaryColumn()
  id: string

  @ManyToOne(() => Comment, (comment) => comment.messages)
  comment: Comment

  @ManyToOne(() => Room, (room) => room.commentMessages)
  room: Room

  @Column('text')
  text: string

  @CreateDateColumn()
  created_at: Date

  @UpdateDateColumn()
  updated_at: Date
}
