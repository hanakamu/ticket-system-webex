import { Module } from '@nestjs/common'
import { ScheduleModule } from '@nestjs/schedule'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Room } from 'src/entity/room.entity'
import { WebexBotModule } from '../webex/bot/module'
import { WebexBotCronService } from './webex/bot/service'

@Module({
  imports: [
    TypeOrmModule.forFeature([Room]),
    ScheduleModule.forRoot(),
    WebexBotModule,
  ],
  providers: [WebexBotCronService],
  exports: [WebexBotCronService],
})
export class CronModule {}
