import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  PrimaryColumn,
} from 'typeorm'
import { Issue } from './issue.entity'
import { Room } from './room.entity'

@Entity()
export class IssueMessage {
  @PrimaryColumn()
  id: string

  @ManyToOne(() => Issue, (issue) => issue.messages)
  issue: Issue

  @ManyToOne(() => Room, (room) => room.commentMessages)
  room: Room

  @Column('text')
  text: string

  @CreateDateColumn()
  created_at: Date

  @UpdateDateColumn()
  updated_at: Date
}
