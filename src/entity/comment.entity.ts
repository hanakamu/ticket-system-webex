import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm'
import { CommentMessage } from './comment-message.entity'
import { Issue } from './issue.entity'

@Entity()
export class Comment {
  @PrimaryColumn()
  id: number

  @ManyToOne(() => Issue, (issue) => issue.comments)
  issue: Issue

  @Column()
  user: string

  @Column('text')
  note: string

  @Column()
  url: string

  @OneToMany(() => CommentMessage, (commentMessage) => commentMessage.comment)
  messages: CommentMessage[]

  @CreateDateColumn()
  created_at: Date

  @UpdateDateColumn()
  updated_at: Date
}
