import { Module } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'
import { WebexBotService } from './service'

@Module({
  imports: [
    HttpModule.register({
      timeout: 6000,
      maxRedirects: 5,
    }),
  ],
  providers: [WebexBotService],
  exports: [WebexBotService],
})
export class WebexBotModule {}
