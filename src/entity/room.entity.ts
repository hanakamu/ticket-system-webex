import {
  Entity,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  PrimaryColumn,
  OneToMany,
} from 'typeorm'
import { IssueMessage } from './issue-message.entity'
import { CommentMessage } from './comment-message.entity'

@Entity()
export class Room {
  @PrimaryColumn()
  id: string

  @Column()
  title: string

  @OneToMany(() => IssueMessage, (issueMessage) => issueMessage.room)
  issueMessages: IssueMessage[]

  @OneToMany(() => CommentMessage, (commentMessage) => commentMessage.room)
  commentMessages: CommentMessage[]

  @CreateDateColumn()
  created_at: Date

  @UpdateDateColumn()
  updated_at: Date
}
