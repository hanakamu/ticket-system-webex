import { Injectable } from '@nestjs/common'
import { HttpService } from '@nestjs/axios'
import { httpsOverHttp } from 'tunnel'
import { firstValueFrom } from 'rxjs'
import { Room } from 'src/entity/room.entity'

@Injectable()
export class WebexBotService {
  private webex_api_url = 'https://api.ciscospark.com:443/v1'
  private webex_api_token: string = process.env.TICKET_WEBEX_TOKEN
  private agent = httpsOverHttp({
    proxy: {
      host: process.env.TICKET_PROXY_HOST,
      port: process.env.TICKET_PROXY_PORT,
    },
  })
  private headers = {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + this.webex_api_token,
  }
  private request_config = {
    httpsAgent: process.env.TICKET_PROXY_HOST != null ? this.agent : null,
    headers: this.headers,
  }

  constructor(private httpService: HttpService) {}

  setToken(token): boolean {
    let result = false
    if (!token) {
      /*TBD: get from DB*/
    } else {
      this.webex_api_token = token
      result = true
    }
    return result
  }

  async getRooms(): Promise<Room[]> {
    const rooms: Room[] = []
    const response = await firstValueFrom(
      this.httpService.get(this.webex_api_url + '/rooms', this.request_config),
    )

    response.data.items.forEach((item) => {
      const room: Room = new Room()
      room.id = item.id
      room.title = item.title
      rooms.push(room)
    })
    return rooms
  }

  async postMessage(room: Room, msg_text: string): Promise<string> {
    let result: string
    const response = await firstValueFrom(
      this.httpService.post(
        this.webex_api_url + '/messages',
        {
          roomId: room.id,
          markdown: msg_text,
        },
        this.request_config,
      ),
    )
    if (response.status == 200) result = response.data.id
    return result
  }

  async postChildMessage(
    room: Room,
    parent_id: string,
    msg_text: string,
  ): Promise<string> {
    let result: string
    const response = await firstValueFrom(
      this.httpService.post(
        this.webex_api_url + '/messages',
        {
          roomId: room.id,
          parentId: parent_id,
          markdown: msg_text,
        },
        this.request_config,
      ),
    )
    if (response.status == 200) result = response.data.id
    return result
  }

  async updateMessage(
    room: Room,
    msg_id: string,
    msg_text: string,
  ): Promise<string> {
    let result: string
    const response = await firstValueFrom(
      this.httpService.put(
        this.webex_api_url + '/messages/' + msg_id,
        {
          roomId: room.id,
          markdown: msg_text,
        },
        this.request_config,
      ),
    )
    if (response.status == 200) result = response.data.id
    return result
  }
}
