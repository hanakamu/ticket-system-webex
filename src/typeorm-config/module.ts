import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'

import { Issue } from '../entity/issue.entity'
import { Comment } from '../entity/comment.entity'
import { Assignee } from '../entity/assignee.entity'
import { Room } from 'src/entity/room.entity'
import { IssueMessage } from 'src/entity/issue-message.entity'
import { CommentMessage } from 'src/entity/comment-message.entity'

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.TICKET_DB_HOST,
      port: Number(process.env.TICKET_DB_PORT),
      username: process.env.TICKET_DB_USER,
      password: process.env.TICKET_DB_PASSWORD,
      database: process.env.TICKET_DB_NAME,
      entities: [Assignee, Issue, Comment, IssueMessage, CommentMessage, Room],
      synchronize: true,
    }),
  ],
  controllers: [],
  providers: [],
})
export class TypeOrmConfigModule {}
