import { Injectable, Logger } from '@nestjs/common'
import { Cron, CronExpression } from '@nestjs/schedule'
import { InjectRepository } from '@nestjs/typeorm'
import { Room } from 'src/entity/room.entity'
import { WebexBotService } from 'src/middleware/webex/bot/service'
import { Repository } from 'typeorm'

@Injectable()
export class WebexBotCronService {
  constructor(
    @InjectRepository(Room)
    private roomRepo: Repository<Room>,

    private webexBotService: WebexBotService,
  ) {}

  private previous_rooms: Room[]
  private readonly logger = new Logger(WebexBotCronService.name)

  @Cron(CronExpression.EVERY_10_SECONDS)
  async handleCron() {
    this.logger.debug('Called every 10 seconds')
    const rooms = await this.webexBotService.getRooms()
    if (!(JSON.stringify(rooms) === JSON.stringify(this.previous_rooms))) {
      const records = await this.roomRepo.find()
      await Promise.all(
        rooms.map(async (room) => {
          const previous_record = await this.roomRepo.findOne({
            id: room.id,
          })
          if (!previous_record) {
            await this.roomRepo.save(room)
          }
        }),
      )
      await Promise.all(
        records.map(async (record) => {
          const current_room = rooms.filter((room) => room.id === record.id)
          if (current_room.length == 0) {
            await this.roomRepo.softDelete(record)
          }
        }),
      )
      this.previous_rooms = rooms
      this.logger.debug('Updated Room Records')
    } else {
      this.logger.debug('Got Same Data')
    }
  }
}
