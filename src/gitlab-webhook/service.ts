import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'

import { WebexBotService } from 'src/middleware/webex/bot/service'

import { Issue } from 'src/entity/issue.entity'
import { Comment } from 'src/entity/comment.entity'
import { Assignee } from 'src/entity/assignee.entity'
import { IssueMessage } from 'src/entity/issue-message.entity'
import { CommentMessage } from 'src/entity/comment-message.entity'
import { Room } from 'src/entity/room.entity'

@Injectable()
export class GitlabWebhookService {
  constructor(
    @InjectRepository(Issue)
    private issueRepo: Repository<Issue>,

    @InjectRepository(Comment)
    private commentRepo: Repository<Comment>,

    @InjectRepository(IssueMessage)
    private issueMessageRepo: Repository<IssueMessage>,

    @InjectRepository(CommentMessage)
    private commentMessageRepo: Repository<CommentMessage>,

    @InjectRepository(Room)
    private roomRepo: Repository<Room>,

    private webexBotService: WebexBotService,
  ) {}

  defaulMessage(): string {
    return 'This route is used for gitlab-webhook.'
  }

  async testWebexIntegration() {
    await this.webexBotService.getRooms()
  }

  private async issueBodyParser(body): Promise<Issue> {
    const issue = new Issue()
    const user = body.user
    const assignees = body.assignees
    const attributes = body.object_attributes
    issue.id = attributes.iid
    issue.url = attributes.url
    issue.state = attributes.state
    issue.action = attributes.action
    issue.title = attributes.title
    issue.description = attributes.description
    issue.user = user.username
    issue.assignees = []
    if (assignees)
      assignees.forEach((element) => {
        const assignee = new Assignee()
        assignee.name = element.username
        issue.assignees.push(assignee)
      })
    return issue
  }

  private async commentBodyParser(body): Promise<Comment> {
    const comment = new Comment()
    const issue = body.issue
    const user = body.user
    const attributes = body.object_attributes
    comment.id = attributes.id
    comment.note = attributes.note
    comment.url = attributes.url
    comment.user = user.username
    comment.issue = await this.issueRepo.findOne({
      id: issue.iid,
    })
    return comment
  }

  private async saveIssue(issue: Issue): Promise<boolean> {
    await this.issueRepo.save(issue)
    return await this.issueRepo.hasId(issue)
  }

  private async saveComment(comment: Comment): Promise<boolean> {
    await this.commentRepo.save(comment)
    return await this.commentRepo.hasId(comment)
  }

  async saveData(body): Promise<boolean> {
    let result = false
    switch (body.object_kind) {
      case 'issue': {
        const issue = await this.issueBodyParser(body)
        result = await this.saveIssue(issue)
        break
      }
      case 'note': {
        if (body.object_attributes.noteable_type == 'Issue') {
          const comment = await this.commentBodyParser(body)
          result = await this.saveComment(comment)
        }
        break
      }
    }
    return result
  }

  private createIssueMessage(issue: Issue): IssueMessage {
    const message = new IssueMessage()
    message.issue = issue
    message.text = ''

    let status_emoji = ''
    switch (issue.state) {
      case 'opened': {
        status_emoji = '🟢'
        break
      }
      case 'closed': {
        status_emoji = '🔴'
      }
    }

    message.text +=
      '## ' +
      status_emoji +
      ' Ticket #' +
      issue.id +
      ' : ' +
      issue.title +
      '\n\n' +
      '--------------\n\n' +
      'Editor: ' +
      issue.user +
      '\n' +
      'URL: ' +
      issue.url +
      '\n' +
      'Assignee: '

    if (issue.assignees) {
      let assignee_text = ''
      issue.assignees.forEach((assignee) => {
        assignee_text +=
          '<@personEmail:' +
          assignee.name +
          '@' +
          process.env.TICKET_USER_EMAIL_DOMAIN +
          '|' +
          assignee.name +
          '> ,'
      })
      message.text +=
        assignee_text.substring(0, assignee_text.length - 1) + '\n'
    }

    message.text +=
      'Description: \n' +
      this.replaceMessageWithMention(issue.description) +
      '\n'

    return message
  }

  private createCommentMessage(comment: Comment): CommentMessage {
    const message = new CommentMessage()
    message.comment = comment
    message.text = ''

    message.text +=
      '## 💬 by ' +
      comment.user +
      '\n\n' +
      '--------------\n\n' +
      'URL: ' +
      comment.url +
      '\n' +
      'Comment: \n' +
      this.replaceMessageWithMention(comment.note) +
      '\n'

    return message
  }

  private replaceMessageWithMention(message_text: string): string {
    const mention_users = message_text.match(/\@[a-z]*/g)
    if (mention_users) {
      mention_users.forEach((user) => {
        const user_name = user.replace('@', '')
        message_text = message_text.replace(
          user,
          '<@personEmail:' +
            user_name +
            '@' +
            process.env.TICKET_USER_EMAIL_DOMAIN +
            '|' +
            user_name +
            '>',
        )
      })
    }
    return message_text
  }

  async sendWebexMessage(body): Promise<boolean> {
    const result = false
    switch (body.object_kind) {
      case 'issue': {
        const issue = await this.issueBodyParser(body)
        await this.sendIssueMessage(issue)
        break
      }
      case 'note': {
        if (body.object_attributes.noteable_type == 'Issue') {
          const comment = await this.commentBodyParser(body)
          await this.sendCommentMessage(comment)
        }
        break
      }
    }
    return result
  }

  async sendIssueMessage(issue: Issue): Promise<boolean> {
    let result = false
    const message = await this.createIssueMessage(issue)
    const rooms = await this.roomRepo.find({ take: 5 })
    await Promise.all(
      rooms.map(async (room) => {
        const previous_message = await this.issueMessageRepo.findOne({
          room: room,
          issue: issue,
        })
        if (previous_message) {
          await this.webexBotService.updateMessage(
            room,
            previous_message.id,
            message.text,
          )
          await this.webexBotService.postChildMessage(
            room,
            previous_message.id,
            '## 📢 **This tickect is ' +
              (message.issue.action == 'update'
                ? 'updated'
                : message.issue.state) +
              '.**',
          )
        } else {
          message.id = await this.webexBotService.postMessage(
            room,
            message.text,
          )
          if (message.id) {
            message.room = room
            await this.issueMessageRepo.save(message)
            result &&= true
          }
        }
      }),
    )
    return result
  }

  async sendCommentMessage(comment: Comment) {
    let result = false
    const message = await this.createCommentMessage(comment)
    const rooms = await this.roomRepo.find({ take: 5 })
    await Promise.all(
      rooms.map(async (room) => {
        const parent_message = await this.issueMessageRepo.findOne({
          room: room,
          issue: comment.issue,
        })
        message.id = await this.webexBotService.postChildMessage(
          room,
          parent_message.id,
          message.text,
        )
        if (message.id) {
          message.room = room
          await this.commentMessageRepo.save(message)
          result &&= true
        }
      }),
    )
    return result
  }
}
